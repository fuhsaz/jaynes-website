<?php

	session_start();

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Upload Step 2</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css" type="text/css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<div class="col-md-8 col-md-offset-2 text-center">
		<form action="upload_2_process.php" method="POST" enctype="multipart/form-data"><form>
			<p> Thumbnail File: 
			<input type="file" name="file" id="file"></p>
			<input type="submit" name="submit" value="Upload Thumbnail">
		</form>
	</div>
</body>
</html>