<?php
	session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Upload step 1</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css" type="text/css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<div class="col-md-8 col-md-offset-2">
<?php
	if (!isset($_SESSION['loggedIn'])) {
		echo "<form action='login.php' method='POST'>";
		echo "<input type='text' name='username' placeholder='username'><br/>";
		echo "<input type='password' name='pass' placeholder='password'><br/>";
		echo "<input type='submit' value='Log In' name='submit'>";
		echo "</form>";
	} else {
?>
		<form action="upload_1_process.php" method="POST" enctype="multipart/form-data"><form>
			<p> Name of the painting: <p>
			<input type="text" name="picName" id="picName"><br/><br>
			<p> Painting file (not the thumbnail, that comes next) 
				<input type="file" name="file" id="file"><p>
			<p> Medium:</p>
			<input type="text" name="medium" id="medium"><br><br>
			<p> Size: (ex 18x24) <br> (if there are halves, type like this "21 1/2 x 21 1/2")</p>
			<input type="text" name="size" id="size"><br><br>
			<input type="submit" name="submit" value="Upload Image">
		</form>
<?php
	}
?>
	</div>
</body>
</html>