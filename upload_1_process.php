<?php
session_start();
$pass = trim(file_get_contents('pass.txt'));
$db = new mysqli('localhost', "Jayne", $pass, "Jayne");

$target_dir = "pictures/";
$target_file = $target_dir . basename($_FILES["file"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["file"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".<br/>";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["file"]["size"] > 4000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["file"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
$_SESSION['name'] = trim($_POST['picName']);
$name = addslashes(trim($_POST['picName']));
$file = addslashes($target_file);
$medium = addslashes(trim($_POST['medium']));

$size = trim($_POST['size']);
if (strpos($size, "/") === false) {
    $size = addslashes($size);
} else {
    $size = str_replace("1/2", "&#189;", $size);
    $size = addslashes($size);
}

$query = "INSERT INTO Paintings (Name, PaintingLocation, Medium, Size) VALUES ('$name',
	'$file', '$medium', '$size')";
$db->query($query) or die("Couldn't insert painting: ".$db->error);

header("Location: upload-2.php");
?>