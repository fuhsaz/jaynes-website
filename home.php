<?php
	session_start();

	//Get the most recent two paintings (based on when they were uploaded)
	$pass = trim(file_get_contents('pass.txt'));
	$db = new mysqli('localhost', "Jayne", $pass, "Jayne");
	$result = $db->query("SELECT * FROM Paintings ORDER BY ID DESC LIMIT 2") or die ("Didn't work ".$db->error);
	$paintings = array();
	while ($row = $result->fetch_assoc()) {
		array_push($paintings, $row);
	}
	$painting1 = $paintings[0];
	$painting1name = $painting1['Name'];
	$painting1location = $painting1['PaintingLocation'];

	$painting2 = $paintings[1];
	$painting2name = $painting2['Name'];
	$painting2location = $painting2['PaintingLocation'];

?>

<!DOCTYPE html>

<html>
<head>
	<title> Home </title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css" type="text/css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

<div class="col-md-8 col-md-offset-2 text-center">
  <h1>Jayne Braxton Fine Art</h1>
</div>

<div class="col-md-8 col-md-offset-2">
	<div class="menubar">
		<a href="home.php"><div class="current-menu-option">Home</div></a>
		<a href="artist_statement.html"><div class="menu-option">Artist Statement</div></a>
		<a href="WorksX.php?pagenum=1"><div class="menu-option">Works</div></a>
		<a href="resume.html"><div class="menu-option">R&eacute;sum&eacute;</div></a>
		<a href="contact.html"><div class="menu-option">Contact Me</div></a>
	</div>
</div>

<div class="col-md-8 col-md-offset-2 site-body">
	<!-- Welcome message or some other stuff on the left-->
	
	

	<div class="col-md-6 welcome">
		<p class="title"> Welcome! </p>
		<p class="home-text">"I found I could say things with color and shapes that I couldn't say any other way-- things I had no words for". <br>
		-Georgia O'Keefe</p>
		<p class="home-text">Thanks for viewing my website, my world.  I'm inspired and transpired by O'Keefe, Matisse, Klimt, Diebenkorn, Hopper and others.
		They have empowered me to speak the language of imagery.</p>	

	</div>

	<!-- Recent works on the right-->
	<div class="col-md-6 text-center recent">
		<p class="title"> Recent Works </p>
<?php
		echo "<span class='description'>$painting1name</span><br/>";
		echo "<a href='painting.php?name=$painting1name'><img src='$painting1location' class='recent-picture'/></a><br/><br/>";

		echo "<span class='description'>$painting2name</span><br/>";
		echo "<a href='painting.php?name=$painting2name'><img src='$painting2location' class='recent-picture'/></a><br/><br/>";
?>
	</div>
	
	
	
</div> 

<div class="col-md-8 col-md-offset-2">
	<a href="upload-1.php">Upload</a>
</div>

</body>
</html>