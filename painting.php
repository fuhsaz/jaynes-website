<?php

	session_start();
	$last_page = $_SESSION['last_page'];

	$db = new mysqli('localhost', "Jayne", "mysupersecurepassword", "Jayne");

	$painting_name = stripslashes($_GET['name']);
	$result = $db->query("SELECT * FROM Paintings WHERE Name='$painting_name'");
	$array = $result->fetch_assoc();
	$loc = $array['PaintingLocation'];
	$medium = $array['Medium'];
	$size = $array['Size'];
	$id = $array['ID'];
	$pageNum = ceil($id/8);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
<?php
	echo "<title>$painting_name</title>";
?>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css" type="text/css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
</head>
<body>

	<div class="col-md-8 col-md-offset-2 text-center">
  		<h1>Jayne Braxton Fine Art</h1>
	</div>
	
	<div class="col-md-8 col-md-offset-2">
		<div class="menubar">
				<a href="home.php"><div class="current-menu-option">Home</div></a>
				<a href="artist_statement.html"><div class="menu-option">Artist Statement</div></a>
<?php	
				echo "<a href='worksX.php?pagenum=$last_page'><div class='menu-option'>Works</div></a>";
?>		
				<a href="resume.html"><div class="menu-option">R&eacute;sum&eacute;</div></a>
				<a href="contact.html"><div class="menu-option">Contact Me</div></a>
<?php
			    echo "<a href='worksX.php?pagenum=$last_page'><div class='back-button'>Back</div></a>";
?>
		</div>
	</div>
	        
			       
			      
	

	<div class="col-md-8 col-md-offset-2">
		<div class="col-md-6 col-md-offset-3 text-center">
<?php
		  	echo "<p class='description'><span class='title'>$painting_name</span><br/> $medium, $size <br/></p>";
?>
		</div>

		<div class="portrait col-md-8 col-md-offset-2">
<?php
		  	echo "<img src='$loc' width=100%/>";
?>
		</div>
	</div>
</body>
</html>