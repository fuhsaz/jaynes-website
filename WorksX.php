<?php
	//New Dynamic works page, generated based on page number
	session_start();
	$pagenum = $_GET['pagenum'];
	$_SESSION['last_page'] = $pagenum;
	$db = new mysqli('localhost', "Jayne", "mysupersecurepassword", "Jayne");
	$result = $db->query("SELECT * FROM Paintings");
	$num_paintings = $result->num_rows;
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css" type="text/css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<div class="col-md-8 col-md-offset-2 text-center">
  		<h1>Jayne Braxton Fine Art</h1>
	</div>
	
	<div class="col-md-8 col-md-offset-2">
		<div class="menubar">
			<a href="home.php"><div class="menu-option">Home</div></a>
			<a href="artist_statement.html"><div class="menu-option">Artist Statement</div></a>
<?php
			        echo "<a href='worksX.php&pagenum=$pagenum'><div class='current-menu-option'>Works</div></a>";
?>   				
			<a href="resume.html"><div class="menu-option">R&eacute;sum&eacute;</div></a>
			<a href="contact.html"><div class="menu-option">Contact Me</div></a>
		</div>
	</div>
    
			      

	<div class="col-md-8 col-md-offset-2 text-center works-body">
		<table>

<?php
			$min = (($pagenum-1)*8 + 1);
			$max = $min + 7;
			if ($max > $num_paintings) {
				$max = $num_paintings;
			}
			$count = 0;
			for ($i = $min; $i <= $max; $i++) {
				if ($i === $min) {
					echo "<tr>";
				}
				if ($i === ($min + 4)) {
					echo "<tr></tr>";
				}
				$query = "SELECT * FROM Paintings WHERE ID=$i";
				$result = $db->query($query) or die ("Painting couldn't be loaded: ".$db->error);
				$array = $result->fetch_assoc();

				$name = $array['Name'];
				$painting_location = stripslashes($array['PaintingLocation']);
				$thumbnail_location = stripslashes($array['ThumbnailLocation']);

				echo "<td><a href='painting.php?name=$name'><img src='$thumbnail_location' class='thumb'></a></td>";
				if ($i === $max) {
					echo "</tr>";
				}
			}
?>



		</table>

		<div class="btn-group" role="group" aria-label="...">

<?php 

			// << button 
			$prevpage = $pagenum-1;
			if ($prevpage < 1) {
				echo "<a href='worksX.php?pagenum=$pagenum' class='btn btn-info' role='button'><<</a>";
			} else {	
				echo "<a href='worksX.php?pagenum=$prevpage' class='btn btn-info' role='button'><<</a>";
			}

			//Now create the buttons for each of the other pages
			$numpages = ceil(($num_paintings / 8.0));
			for ($i = 1; $i <= $numpages; $i++) {
				if ($i === $pagenum) {
					echo "<a href='worksX.php?pagenum=$pagenum' class='btn btn-info active' role='button'>$i</a>";
				} else {
					echo "<a href='worksX.php?pagenum=$i' class='btn btn-info' role='button'>$i</a>";
				}
			}
		  	
		  	// >> button
		  	$nextpage = $pagenum+1;
		  	if ($nextpage > $numpages) {
				echo "<a href='worksX.php?pagenum=$pagenum' class='btn btn-info' role='button'>>></a>";
			} else {
				
				echo "<a href='worksX.php?pagenum=$nextpage' class='btn btn-info' role='button'>>></a>";
			}

?>

		</div>
	</div>

	
</body>
</html>